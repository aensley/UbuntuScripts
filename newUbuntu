#!/usr/bin/env bash

# Performs common setup steps on a new Installation.

toRemove=''
toInstall=''
pythonInstalled=''
gnomeExtensionsInstalled=''
sensorsInstalled=''

echoHugeHeader () {
  echo
  echo -e "====================  \033[32m\033[1m$1\033[m ==================== "
  echo
}

removeGames() {
  toRemove="${toRemove} aisleriot* cheese gnome-mahjongg gnome-mines gnome-sudoku"
}

removeTracking() {
  toRemove="${toRemove} activity-log-manager apport zeitgeist-datahub"
}

removeLibreOffice() {
  toRemove="${toRemove} libreoffice*"
}

removeMedia() {
  toRemove="${toRemove} rhythmbox* simple-scan totem*"
}

removeMisc() {
  toRemove="${toRemove} deja-dup* remmina* thunderbird* transmission* vino* xserver-xorg-input-wacom"
  sudo rm /usr/share/applications/gnome-wacom-panel.desktop
}

removeApps(){
  echoHugeHeader "Removing Apps"
  sudo apt purge -y -f ${toRemove} && sudo apt -y -f --purge autoremove
}

installTweaks() {
  toInstall="${toInstall} gnome-tweak-tool"
}

installGnomeExtensions() {
  toInstall="${toInstall} chrome-gnome-shell gnome-shell-extensions"
  gnomeExtensionsInstalled='1'
}

installSensors() {
  toInstall="${toInstall} lm-sensors hddtemp gir1.2-gtop-2.0 gir1.2-networkmanager-1.0  gir1.2-clutter-1.0"
  sensorsInstalled='1'
}

installVlc() {
  toInstall="${toInstall} vlc"
}

installChrome() {
  echoHugeHeader "Installing Chrome"
  wget -O /tmp/chrome.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo dpkg -i /tmp/chrome.deb
  rm /tmp/chrome.deb
}

installChromium() {
  toInstall="${toInstall} chromium-browser"
}

installBrave() {
  echoHugeHeader "Installing Brave"
  wget -qO- https://s3-us-west-2.amazonaws.com/brave-apt/keys.asc | sudo apt-key add -
  echo "deb [arch=amd64] https://s3-us-west-2.amazonaws.com/brave-apt `lsb_release -sc` main" | sudo tee -a /etc/apt/sources.list.d/brave-`lsb_release -sc`.list
  toInstall="${toInstall} brave"
}

installLibreOfficeFresh() {
  echoHugeHeader "Installing LibreOffice Fresh"
  sudo add-apt-repository -y ppa:libreoffice/ppa
  toInstall="${toInstall} libreoffice-calc libreoffice-gnome libreoffice-impress libreoffice-pdfimport libreoffice-style-* libreoffice-writer"
}

installGimp() {
  toInstall="${toInstall} gimp gimp-data-extras gimp-plugin-registry"
}

installEnpass() {
  echoHugeHeader "Installing Enpass"
  echo "deb http://repo.sinew.in/ stable main" | sudo tee /etc/apt/sources.list.d/enpass.list
  wget -O - https://dl.sinew.in/keys/enpass-linux.key | sudo apt-key add -
  toInstall="${toInstall} enpass"
}

installGeditPlugins() {
  toInstall="${toInstall} gedit-plugins gedit-developer-plugins gedit-plugin-text-size gedit-source-code-browser-plugin"
}

installJre() {
  toInstall="${toInstall} openjdk-9-jre"
}

installJdk() {
  toInstall="${toInstall} openjdk-9-jdk"
}

installPhp() {
  echoHugeHeader "Installing PHP"
  sudo add-apt-repository -y ppa:ondrej/php
  toInstall="${toInstall} php-cli php-curl php-json php-mbstring php-mcrypt php-xml php-zip"
}

installNodejs() {
  echoHugeHeader "Installing Node.js"
  wget -qO - https://deb.nodesource.com/setup_8.x | sudo -E bash -
  toInstall="${toInstall} nodejs"
}

installPython() {
  pythonInstalled='1'
  toInstall="${toInstall} python-pip"
}

installGitHubBackup() {
  if [ -n "${pythonInstalled}" ]; then
    sudo pip install -U git+git://github.com/josegonzalez/python-github-backup.git#egg=github-backup
  fi
}

installApps(){
  echoHugeHeader "Installing Apps"
  sudo apt update && sudo apt -y -f install ${toInstall}
}

echoHugeHeader "Checking Prereqs"
sudo apt update && sudo apt -y -f install git

echoHugeHeader "Getting Scripts"
wget -qO- https://raw.githubusercontent.com/aensley/UbuntuScripts/master/sbin/updateScripts | bash -

removeChoices=$(
  whiptail --title "Select Software to Remove" --checklist "Select Software to Remove" 10 40 5 \
    Games       "Games"         on \
    Tracking    "Tracking"      on \
    LibreOffice "LibreOffice"   off \
    Media       "Media"         on \
    Misc        "Miscellaneous" on \
    3>&1 1>&2 2>&3
)

installChoices=$(
  whiptail --title "Select Software to Install" --checklist "Select Software to Install" 23 50 17 \
    Tweaks           "Gnome Tweak Tool"  on \
    GnomeExtensions  "Gnome Extensions"  on \
    Sensors          "Sensors"           on \
    Vlc              "VLC"               on \
    Chrome           "Chrome"            on \
    Chromium         "Chromium"          on \
    Brave            "Brave"             on \
    LibreOfficeFresh "LibreOffice Fresh" on \
    Gimp             "GIMP"              off \
    Enpass           "Enpass"            off \
    GeditPlugins     "Gedit Plugins"     off \
    Jre              "JRE"               off \
    Jdk              "JDK"               off \
    Php              "PHP"               off \
    Nodejs           "Node.js"           off \
    Python           "Python"            off \
    GitHubBackup     "GitHub Backup"     off \
    3>&1 1>&2 2>&3
)

for removeJob in ${removeChoices}; do
  eval "remove${removeJob}"
done

removeApps

echoHugeHeader "Upgrading System"
sudo apt update && sudo apt -y -f -u full-upgrade && sudo apt autoremove --purge && sudo apt clean

for installJob in ${installChoices}; do
  eval "install${installJob}"
done

installApps

if [ -n "${sensorsInstalled}" ]; then
  echoHugeHeader "Setting up Sensors"
  sudo sensors-detect
  sudo /etc/init.d/kmod start
fi

echoHugeHeader "Upgrading System"
/usr/local/sbin/upgrade

if [ -n "${gnomeExtensionsInstalled}" ]; then
  echoHugeHeader "Opening favorite Gnome extensions"
  read -p "Please enter your email address for profile Gravatar: " gravatarEmail
  dconf write /org/gnome/shell/extensions/gravatar/email "'${gravatarEmail}'"
  xdg-open "https://extensions.gnome.org/extension/1262/bing-wallpaper-changer/"
  xdg-open "https://extensions.gnome.org/extension/1015/gravatar/"
  xdg-open "https://extensions.gnome.org/extension/750/openweather/"
  xdg-open "https://extensions.gnome.org/extension/8/places-status-indicator/"
  xdg-open "https://extensions.gnome.org/extension/7/removable-drive-menu/"
  xdg-open "https://extensions.gnome.org/extension/1145/sensory-perception/"
  xdg-open "https://extensions.gnome.org/extension/120/system-monitor/"
fi
